
				***Binary Search Tree***
		40201757 - Dimitar Hristov - BSc (Hons) Computing Science
		
The Binary Search Tree is a data structure that stores integer data for fast retrieval in order. 
This project consists of 'binary_tree.h' file in which the node and Binary Search Tree constructors are defined with associated functions and operator overloads.

The methods used in the implementation of the Binary Search Tree are:
	* binary tree() - Constructor that creates an empty binary tree.
	* binary tree(int value) - Constructor that creates a binary tree with an initial value to store.
	* binary tree(const vector<int> &values) - Constructor that creates a binary tree from a collection of existing values.
	* binary tree(const binary tree &rhs) - Constructor that creates a binary tree by copying an existing binary tree.
	* ~binary tree() - Destructor that destroys the binary tree, cleaning up used resources.
	* void insert(int value) - Adds a value to the binary tree.
	* void remove(int value) - Removes a value from the binary tree if it exists.
	* bool exists(int value) - Returns true if the value is in the tree, false otherwise.
	* string inorder() - Generates a string representing the tree in numerical order.
	* string preorder() - Generates a string representing the tree in pre-order fashion.
	* string postorder() - Generates a string representing the tree in post-order fashion.
	* '=' operator - Copy assignment operator. Copies the tree to another tree.
	* '==' operator - Checks if two trees are equal.
	* '!=' operator - Checks if two trees are not equal.
	* '+' operator - Inserts a new value into the binary tree.
	* '-' operator - Removes a value from the binary tree.
	* '>>' operator - Reads in values from an input stream into the tree.
	* '<<' operator - Writes the values, in-order, to an output stream.
	
HOW TO USE IT:
If you want only to compile the files, you can type in the compiler "nmake compile".
This instruction builds the binary tree as a separate library and also allows building of the test application against this library.
If you want to see how the tests are working, you can type in "nmake binary_tree". 
